﻿namespace ProjetoADMPersonalizadora.Login.Menu.Cliente
{
    partial class frmCadastrarCliente
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblTelefone_C = new System.Windows.Forms.Label();
            this.txtTelefone_C = new System.Windows.Forms.MaskedTextBox();
            this.lblCPF_C = new System.Windows.Forms.Label();
            this.txtCPF_C = new System.Windows.Forms.MaskedTextBox();
            this.txtUsuario_C = new System.Windows.Forms.MaskedTextBox();
            this.btnCadastrar_C = new System.Windows.Forms.Button();
            this.lblEmail_C = new System.Windows.Forms.Label();
            this.txtEmail_C = new System.Windows.Forms.MaskedTextBox();
            this.DT_Nascimento_C = new System.Windows.Forms.DateTimePicker();
            this.lblData_Nascimento_C = new System.Windows.Forms.Label();
            this.lblComplemento_C = new System.Windows.Forms.Label();
            this.txtComplemento_C = new System.Windows.Forms.MaskedTextBox();
            this.lblNumero_C = new System.Windows.Forms.Label();
            this.txtNumero_C = new System.Windows.Forms.MaskedTextBox();
            this.lblNome_C = new System.Windows.Forms.Label();
            this.txtNome_C = new System.Windows.Forms.MaskedTextBox();
            this.lblCadastrarCliente = new System.Windows.Forms.Label();
            this.lblCEP_C = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblTelefone_C
            // 
            this.lblTelefone_C.AutoSize = true;
            this.lblTelefone_C.BackColor = System.Drawing.Color.Transparent;
            this.lblTelefone_C.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.lblTelefone_C.Location = new System.Drawing.Point(38, 81);
            this.lblTelefone_C.Name = "lblTelefone_C";
            this.lblTelefone_C.Size = new System.Drawing.Size(80, 21);
            this.lblTelefone_C.TabIndex = 73;
            this.lblTelefone_C.Text = "Telefone:";
            // 
            // txtTelefone_C
            // 
            this.txtTelefone_C.Location = new System.Drawing.Point(126, 82);
            this.txtTelefone_C.Name = "txtTelefone_C";
            this.txtTelefone_C.Size = new System.Drawing.Size(143, 20);
            this.txtTelefone_C.TabIndex = 72;
            // 
            // lblCPF_C
            // 
            this.lblCPF_C.AutoSize = true;
            this.lblCPF_C.BackColor = System.Drawing.Color.Transparent;
            this.lblCPF_C.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.lblCPF_C.Location = new System.Drawing.Point(359, 132);
            this.lblCPF_C.Name = "lblCPF_C";
            this.lblCPF_C.Size = new System.Drawing.Size(44, 21);
            this.lblCPF_C.TabIndex = 67;
            this.lblCPF_C.Text = "CPF:";
            // 
            // txtCPF_C
            // 
            this.txtCPF_C.Location = new System.Drawing.Point(409, 132);
            this.txtCPF_C.Name = "txtCPF_C";
            this.txtCPF_C.Size = new System.Drawing.Size(148, 20);
            this.txtCPF_C.TabIndex = 66;
            // 
            // txtUsuario_C
            // 
            this.txtUsuario_C.Location = new System.Drawing.Point(409, 58);
            this.txtUsuario_C.Name = "txtUsuario_C";
            this.txtUsuario_C.Size = new System.Drawing.Size(148, 20);
            this.txtUsuario_C.TabIndex = 64;
            // 
            // btnCadastrar_C
            // 
            this.btnCadastrar_C.Font = new System.Drawing.Font("Century Gothic", 20F);
            this.btnCadastrar_C.Location = new System.Drawing.Point(212, 171);
            this.btnCadastrar_C.Name = "btnCadastrar_C";
            this.btnCadastrar_C.Size = new System.Drawing.Size(167, 42);
            this.btnCadastrar_C.TabIndex = 63;
            this.btnCadastrar_C.Text = "Cadastrar";
            this.btnCadastrar_C.UseVisualStyleBackColor = true;
            // 
            // lblEmail_C
            // 
            this.lblEmail_C.AutoSize = true;
            this.lblEmail_C.BackColor = System.Drawing.Color.Transparent;
            this.lblEmail_C.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.lblEmail_C.Location = new System.Drawing.Point(60, 133);
            this.lblEmail_C.Name = "lblEmail_C";
            this.lblEmail_C.Size = new System.Drawing.Size(60, 21);
            this.lblEmail_C.TabIndex = 62;
            this.lblEmail_C.Text = "E-mail:";
            // 
            // txtEmail_C
            // 
            this.txtEmail_C.Location = new System.Drawing.Point(126, 134);
            this.txtEmail_C.Name = "txtEmail_C";
            this.txtEmail_C.Size = new System.Drawing.Size(143, 20);
            this.txtEmail_C.TabIndex = 61;
            // 
            // DT_Nascimento_C
            // 
            this.DT_Nascimento_C.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.DT_Nascimento_C.Location = new System.Drawing.Point(126, 108);
            this.DT_Nascimento_C.MaxDate = new System.DateTime(5100, 12, 31, 0, 0, 0, 0);
            this.DT_Nascimento_C.Name = "DT_Nascimento_C";
            this.DT_Nascimento_C.Size = new System.Drawing.Size(143, 20);
            this.DT_Nascimento_C.TabIndex = 60;
            // 
            // lblData_Nascimento_C
            // 
            this.lblData_Nascimento_C.AutoSize = true;
            this.lblData_Nascimento_C.BackColor = System.Drawing.Color.Transparent;
            this.lblData_Nascimento_C.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.lblData_Nascimento_C.Location = new System.Drawing.Point(12, 108);
            this.lblData_Nascimento_C.Name = "lblData_Nascimento_C";
            this.lblData_Nascimento_C.Size = new System.Drawing.Size(108, 21);
            this.lblData_Nascimento_C.TabIndex = 59;
            this.lblData_Nascimento_C.Text = "Nascimento:";
            // 
            // lblComplemento_C
            // 
            this.lblComplemento_C.AutoSize = true;
            this.lblComplemento_C.BackColor = System.Drawing.Color.Transparent;
            this.lblComplemento_C.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.lblComplemento_C.Location = new System.Drawing.Point(275, 107);
            this.lblComplemento_C.Name = "lblComplemento_C";
            this.lblComplemento_C.Size = new System.Drawing.Size(128, 21);
            this.lblComplemento_C.TabIndex = 58;
            this.lblComplemento_C.Text = "Complemento:";
            // 
            // txtComplemento_C
            // 
            this.txtComplemento_C.Location = new System.Drawing.Point(409, 108);
            this.txtComplemento_C.Name = "txtComplemento_C";
            this.txtComplemento_C.Size = new System.Drawing.Size(148, 20);
            this.txtComplemento_C.TabIndex = 57;
            // 
            // lblNumero_C
            // 
            this.lblNumero_C.AutoSize = true;
            this.lblNumero_C.BackColor = System.Drawing.Color.Transparent;
            this.lblNumero_C.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.lblNumero_C.Location = new System.Drawing.Point(327, 83);
            this.lblNumero_C.Name = "lblNumero_C";
            this.lblNumero_C.Size = new System.Drawing.Size(76, 21);
            this.lblNumero_C.TabIndex = 56;
            this.lblNumero_C.Text = "Número:";
            // 
            // txtNumero_C
            // 
            this.txtNumero_C.Location = new System.Drawing.Point(409, 84);
            this.txtNumero_C.Name = "txtNumero_C";
            this.txtNumero_C.Size = new System.Drawing.Size(148, 20);
            this.txtNumero_C.TabIndex = 55;
            // 
            // lblNome_C
            // 
            this.lblNome_C.AutoSize = true;
            this.lblNome_C.BackColor = System.Drawing.Color.Transparent;
            this.lblNome_C.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.lblNome_C.Location = new System.Drawing.Point(59, 55);
            this.lblNome_C.Name = "lblNome_C";
            this.lblNome_C.Size = new System.Drawing.Size(61, 21);
            this.lblNome_C.TabIndex = 54;
            this.lblNome_C.Text = "Nome:";
            // 
            // txtNome_C
            // 
            this.txtNome_C.Location = new System.Drawing.Point(126, 56);
            this.txtNome_C.Name = "txtNome_C";
            this.txtNome_C.Size = new System.Drawing.Size(143, 20);
            this.txtNome_C.TabIndex = 53;
            // 
            // lblCadastrarCliente
            // 
            this.lblCadastrarCliente.AutoSize = true;
            this.lblCadastrarCliente.Font = new System.Drawing.Font("Century Gothic", 24F, System.Drawing.FontStyle.Bold);
            this.lblCadastrarCliente.ForeColor = System.Drawing.Color.DarkTurquoise;
            this.lblCadastrarCliente.Location = new System.Drawing.Point(154, 9);
            this.lblCadastrarCliente.Name = "lblCadastrarCliente";
            this.lblCadastrarCliente.Size = new System.Drawing.Size(293, 38);
            this.lblCadastrarCliente.TabIndex = 52;
            this.lblCadastrarCliente.Text = "Cadastrar cliente:";
            // 
            // lblCEP_C
            // 
            this.lblCEP_C.AutoSize = true;
            this.lblCEP_C.BackColor = System.Drawing.Color.Transparent;
            this.lblCEP_C.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.lblCEP_C.Location = new System.Drawing.Point(358, 59);
            this.lblCEP_C.Name = "lblCEP_C";
            this.lblCEP_C.Size = new System.Drawing.Size(45, 21);
            this.lblCEP_C.TabIndex = 65;
            this.lblCEP_C.Text = "CEP:";
            // 
            // frmCadastrarCliente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(576, 225);
            this.Controls.Add(this.lblTelefone_C);
            this.Controls.Add(this.txtTelefone_C);
            this.Controls.Add(this.lblCPF_C);
            this.Controls.Add(this.txtCPF_C);
            this.Controls.Add(this.lblCEP_C);
            this.Controls.Add(this.txtUsuario_C);
            this.Controls.Add(this.btnCadastrar_C);
            this.Controls.Add(this.lblEmail_C);
            this.Controls.Add(this.txtEmail_C);
            this.Controls.Add(this.DT_Nascimento_C);
            this.Controls.Add(this.lblData_Nascimento_C);
            this.Controls.Add(this.lblComplemento_C);
            this.Controls.Add(this.txtComplemento_C);
            this.Controls.Add(this.lblNumero_C);
            this.Controls.Add(this.txtNumero_C);
            this.Controls.Add(this.lblNome_C);
            this.Controls.Add(this.txtNome_C);
            this.Controls.Add(this.lblCadastrarCliente);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmCadastrarCliente";
            this.Text = "frmCadastrarCliente";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblTelefone_C;
        private System.Windows.Forms.MaskedTextBox txtTelefone_C;
        private System.Windows.Forms.Label lblCPF_C;
        private System.Windows.Forms.MaskedTextBox txtCPF_C;
        private System.Windows.Forms.MaskedTextBox txtUsuario_C;
        private System.Windows.Forms.Button btnCadastrar_C;
        private System.Windows.Forms.Label lblEmail_C;
        private System.Windows.Forms.MaskedTextBox txtEmail_C;
        private System.Windows.Forms.DateTimePicker DT_Nascimento_C;
        private System.Windows.Forms.Label lblData_Nascimento_C;
        private System.Windows.Forms.Label lblComplemento_C;
        private System.Windows.Forms.MaskedTextBox txtComplemento_C;
        private System.Windows.Forms.Label lblNumero_C;
        private System.Windows.Forms.MaskedTextBox txtNumero_C;
        private System.Windows.Forms.Label lblNome_C;
        private System.Windows.Forms.MaskedTextBox txtNome_C;
        private System.Windows.Forms.Label lblCadastrarCliente;
        private System.Windows.Forms.Label lblCEP_C;
    }
}