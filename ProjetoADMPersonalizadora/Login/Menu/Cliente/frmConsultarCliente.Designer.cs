﻿namespace ProjetoADMPersonalizadora.Login.Menu.Cliente
{
    partial class frmConsultarCliente
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnPesquisar_fun = new System.Windows.Forms.Button();
            this.txtConNome_C = new System.Windows.Forms.TextBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.lblConNome_C = new System.Windows.Forms.Label();
            this.lblCon_Fornecedor = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.lblCon_CPF_C = new System.Windows.Forms.Label();
            this.lblConsultarCliente = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnPesquisar_fun
            // 
            this.btnPesquisar_fun.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.btnPesquisar_fun.Location = new System.Drawing.Point(412, 51);
            this.btnPesquisar_fun.Name = "btnPesquisar_fun";
            this.btnPesquisar_fun.Size = new System.Drawing.Size(89, 47);
            this.btnPesquisar_fun.TabIndex = 75;
            this.btnPesquisar_fun.Text = "Pesquisar";
            this.btnPesquisar_fun.UseVisualStyleBackColor = true;
            // 
            // txtConNome_C
            // 
            this.txtConNome_C.Location = new System.Drawing.Point(236, 52);
            this.txtConNome_C.Name = "txtConNome_C";
            this.txtConNome_C.Size = new System.Drawing.Size(170, 20);
            this.txtConNome_C.TabIndex = 74;
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(25, 104);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(607, 278);
            this.dataGridView1.TabIndex = 73;
            // 
            // lblConNome_C
            // 
            this.lblConNome_C.AutoSize = true;
            this.lblConNome_C.BackColor = System.Drawing.Color.Transparent;
            this.lblConNome_C.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblConNome_C.Location = new System.Drawing.Point(169, 51);
            this.lblConNome_C.Name = "lblConNome_C";
            this.lblConNome_C.Size = new System.Drawing.Size(61, 21);
            this.lblConNome_C.TabIndex = 72;
            this.lblConNome_C.Text = "Nome:";
            // 
            // lblCon_Fornecedor
            // 
            this.lblCon_Fornecedor.AutoSize = true;
            this.lblCon_Fornecedor.Font = new System.Drawing.Font("Century Gothic", 24F, System.Drawing.FontStyle.Bold);
            this.lblCon_Fornecedor.ForeColor = System.Drawing.Color.DarkTurquoise;
            this.lblCon_Fornecedor.Location = new System.Drawing.Point(187, -46);
            this.lblCon_Fornecedor.Name = "lblCon_Fornecedor";
            this.lblCon_Fornecedor.Size = new System.Drawing.Size(235, 38);
            this.lblCon_Fornecedor.TabIndex = 71;
            this.lblCon_Fornecedor.Text = "Fornecedores:";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(236, 78);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(170, 20);
            this.textBox1.TabIndex = 77;
            // 
            // lblCon_CPF_C
            // 
            this.lblCon_CPF_C.AutoSize = true;
            this.lblCon_CPF_C.BackColor = System.Drawing.Color.Transparent;
            this.lblCon_CPF_C.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCon_CPF_C.Location = new System.Drawing.Point(169, 77);
            this.lblCon_CPF_C.Name = "lblCon_CPF_C";
            this.lblCon_CPF_C.Size = new System.Drawing.Size(44, 21);
            this.lblCon_CPF_C.TabIndex = 76;
            this.lblCon_CPF_C.Text = "CPF:";
            // 
            // lblConsultarCliente
            // 
            this.lblConsultarCliente.AutoSize = true;
            this.lblConsultarCliente.Font = new System.Drawing.Font("Century Gothic", 24F, System.Drawing.FontStyle.Bold);
            this.lblConsultarCliente.ForeColor = System.Drawing.Color.DarkTurquoise;
            this.lblConsultarCliente.Location = new System.Drawing.Point(183, 9);
            this.lblConsultarCliente.Name = "lblConsultarCliente";
            this.lblConsultarCliente.Size = new System.Drawing.Size(286, 38);
            this.lblConsultarCliente.TabIndex = 78;
            this.lblConsultarCliente.Text = "Consultar cliente:";
            // 
            // frmConsultarCliente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(658, 394);
            this.Controls.Add(this.lblConsultarCliente);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.lblCon_CPF_C);
            this.Controls.Add(this.btnPesquisar_fun);
            this.Controls.Add(this.txtConNome_C);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.lblConNome_C);
            this.Controls.Add(this.lblCon_Fornecedor);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmConsultarCliente";
            this.Text = "frmConsultarCliente";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnPesquisar_fun;
        private System.Windows.Forms.TextBox txtConNome_C;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label lblConNome_C;
        private System.Windows.Forms.Label lblCon_Fornecedor;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label lblCon_CPF_C;
        private System.Windows.Forms.Label lblConsultarCliente;
    }
}