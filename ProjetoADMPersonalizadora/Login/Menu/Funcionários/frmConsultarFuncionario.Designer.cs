﻿namespace ProjetoADMPersonalizadora.Login.Menu.Funcionários
{
    partial class frmConsultarFuncionario
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblConsultarCliente = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.lblCon_CPF_F = new System.Windows.Forms.Label();
            this.btnPesquisar_fun = new System.Windows.Forms.Button();
            this.txtConNome_C = new System.Windows.Forms.TextBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.lblConNome_F = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // lblConsultarCliente
            // 
            this.lblConsultarCliente.AutoSize = true;
            this.lblConsultarCliente.Font = new System.Drawing.Font("Century Gothic", 24F, System.Drawing.FontStyle.Bold);
            this.lblConsultarCliente.ForeColor = System.Drawing.Color.DarkTurquoise;
            this.lblConsultarCliente.Location = new System.Drawing.Point(170, 9);
            this.lblConsultarCliente.Name = "lblConsultarCliente";
            this.lblConsultarCliente.Size = new System.Drawing.Size(354, 38);
            this.lblConsultarCliente.TabIndex = 85;
            this.lblConsultarCliente.Text = "Consultar funcionario:";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(252, 78);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(170, 20);
            this.textBox1.TabIndex = 84;
            // 
            // lblCon_CPF_F
            // 
            this.lblCon_CPF_F.AutoSize = true;
            this.lblCon_CPF_F.BackColor = System.Drawing.Color.Transparent;
            this.lblCon_CPF_F.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCon_CPF_F.Location = new System.Drawing.Point(185, 77);
            this.lblCon_CPF_F.Name = "lblCon_CPF_F";
            this.lblCon_CPF_F.Size = new System.Drawing.Size(44, 21);
            this.lblCon_CPF_F.TabIndex = 83;
            this.lblCon_CPF_F.Text = "CPF:";
            // 
            // btnPesquisar_fun
            // 
            this.btnPesquisar_fun.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.btnPesquisar_fun.Location = new System.Drawing.Point(428, 51);
            this.btnPesquisar_fun.Name = "btnPesquisar_fun";
            this.btnPesquisar_fun.Size = new System.Drawing.Size(96, 47);
            this.btnPesquisar_fun.TabIndex = 82;
            this.btnPesquisar_fun.Text = "Pesquisar";
            this.btnPesquisar_fun.UseVisualStyleBackColor = true;
            // 
            // txtConNome_C
            // 
            this.txtConNome_C.Location = new System.Drawing.Point(252, 52);
            this.txtConNome_C.Name = "txtConNome_C";
            this.txtConNome_C.Size = new System.Drawing.Size(170, 20);
            this.txtConNome_C.TabIndex = 81;
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(12, 109);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(685, 278);
            this.dataGridView1.TabIndex = 80;
            // 
            // lblConNome_F
            // 
            this.lblConNome_F.AutoSize = true;
            this.lblConNome_F.BackColor = System.Drawing.Color.Transparent;
            this.lblConNome_F.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblConNome_F.Location = new System.Drawing.Point(185, 51);
            this.lblConNome_F.Name = "lblConNome_F";
            this.lblConNome_F.Size = new System.Drawing.Size(61, 21);
            this.lblConNome_F.TabIndex = 79;
            this.lblConNome_F.Text = "Nome:";
            // 
            // frmConsultarFuncionario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(709, 399);
            this.Controls.Add(this.lblConsultarCliente);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.lblCon_CPF_F);
            this.Controls.Add(this.btnPesquisar_fun);
            this.Controls.Add(this.txtConNome_C);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.lblConNome_F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmConsultarFuncionario";
            this.Text = "frmConsultarFuncionario";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblConsultarCliente;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label lblCon_CPF_F;
        private System.Windows.Forms.Button btnPesquisar_fun;
        private System.Windows.Forms.TextBox txtConNome_C;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label lblConNome_F;
    }
}