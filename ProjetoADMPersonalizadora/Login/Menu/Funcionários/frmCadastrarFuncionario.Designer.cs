﻿namespace ProjetoADMPersonalizadora.Login.Menu.Funcionários
{
    partial class frmCadastrarFuncionario
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCadastrar_F = new System.Windows.Forms.Button();
            this.lblEmail_F = new System.Windows.Forms.Label();
            this.txtEmail_F = new System.Windows.Forms.MaskedTextBox();
            this.DT_Nascimento = new System.Windows.Forms.DateTimePicker();
            this.lblData_Nascimento_F = new System.Windows.Forms.Label();
            this.lblSenha_F = new System.Windows.Forms.Label();
            this.txtSenha_F = new System.Windows.Forms.MaskedTextBox();
            this.lblC_Senha_F = new System.Windows.Forms.Label();
            this.txtC_Senha_F = new System.Windows.Forms.MaskedTextBox();
            this.lblNome_F = new System.Windows.Forms.Label();
            this.txtNome_F = new System.Windows.Forms.MaskedTextBox();
            this.lblCadastrarCliente = new System.Windows.Forms.Label();
            this.lblUsuario_F = new System.Windows.Forms.Label();
            this.txtUsuario_F = new System.Windows.Forms.MaskedTextBox();
            this.lblCargo_F = new System.Windows.Forms.Label();
            this.lblCPF_F = new System.Windows.Forms.Label();
            this.maskedTextBox2 = new System.Windows.Forms.MaskedTextBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.lblSexo = new System.Windows.Forms.Label();
            this.cboPermissao_Func = new System.Windows.Forms.ComboBox();
            this.lblPermissao_Func = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnCadastrar_F
            // 
            this.btnCadastrar_F.Font = new System.Drawing.Font("Century Gothic", 20F);
            this.btnCadastrar_F.Location = new System.Drawing.Point(248, 204);
            this.btnCadastrar_F.Name = "btnCadastrar_F";
            this.btnCadastrar_F.Size = new System.Drawing.Size(167, 42);
            this.btnCadastrar_F.TabIndex = 39;
            this.btnCadastrar_F.Text = "Cadastrar";
            this.btnCadastrar_F.UseVisualStyleBackColor = true;
            // 
            // lblEmail_F
            // 
            this.lblEmail_F.AutoSize = true;
            this.lblEmail_F.BackColor = System.Drawing.Color.Transparent;
            this.lblEmail_F.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.lblEmail_F.Location = new System.Drawing.Point(97, 87);
            this.lblEmail_F.Name = "lblEmail_F";
            this.lblEmail_F.Size = new System.Drawing.Size(60, 21);
            this.lblEmail_F.TabIndex = 35;
            this.lblEmail_F.Text = "E-mail:";
            // 
            // txtEmail_F
            // 
            this.txtEmail_F.Location = new System.Drawing.Point(163, 88);
            this.txtEmail_F.Name = "txtEmail_F";
            this.txtEmail_F.Size = new System.Drawing.Size(148, 20);
            this.txtEmail_F.TabIndex = 34;
            // 
            // DT_Nascimento
            // 
            this.DT_Nascimento.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.DT_Nascimento.Location = new System.Drawing.Point(499, 62);
            this.DT_Nascimento.MaxDate = new System.DateTime(5100, 12, 31, 0, 0, 0, 0);
            this.DT_Nascimento.Name = "DT_Nascimento";
            this.DT_Nascimento.Size = new System.Drawing.Size(115, 20);
            this.DT_Nascimento.TabIndex = 33;
            // 
            // lblData_Nascimento_F
            // 
            this.lblData_Nascimento_F.AutoSize = true;
            this.lblData_Nascimento_F.BackColor = System.Drawing.Color.Transparent;
            this.lblData_Nascimento_F.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.lblData_Nascimento_F.Location = new System.Drawing.Point(317, 62);
            this.lblData_Nascimento_F.Name = "lblData_Nascimento_F";
            this.lblData_Nascimento_F.Size = new System.Drawing.Size(176, 21);
            this.lblData_Nascimento_F.TabIndex = 32;
            this.lblData_Nascimento_F.Text = "Data de nascimento:";
            // 
            // lblSenha_F
            // 
            this.lblSenha_F.AutoSize = true;
            this.lblSenha_F.BackColor = System.Drawing.Color.Transparent;
            this.lblSenha_F.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.lblSenha_F.Location = new System.Drawing.Point(94, 139);
            this.lblSenha_F.Name = "lblSenha_F";
            this.lblSenha_F.Size = new System.Drawing.Size(63, 21);
            this.lblSenha_F.TabIndex = 31;
            this.lblSenha_F.Text = "Senha:";
            // 
            // txtSenha_F
            // 
            this.txtSenha_F.Location = new System.Drawing.Point(163, 140);
            this.txtSenha_F.Name = "txtSenha_F";
            this.txtSenha_F.Size = new System.Drawing.Size(148, 20);
            this.txtSenha_F.TabIndex = 30;
            // 
            // lblC_Senha_F
            // 
            this.lblC_Senha_F.AutoSize = true;
            this.lblC_Senha_F.BackColor = System.Drawing.Color.Transparent;
            this.lblC_Senha_F.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.lblC_Senha_F.Location = new System.Drawing.Point(15, 165);
            this.lblC_Senha_F.Name = "lblC_Senha_F";
            this.lblC_Senha_F.Size = new System.Drawing.Size(142, 21);
            this.lblC_Senha_F.TabIndex = 29;
            this.lblC_Senha_F.Text = "Confirmar senha:";
            // 
            // txtC_Senha_F
            // 
            this.txtC_Senha_F.Location = new System.Drawing.Point(163, 166);
            this.txtC_Senha_F.Name = "txtC_Senha_F";
            this.txtC_Senha_F.Size = new System.Drawing.Size(148, 20);
            this.txtC_Senha_F.TabIndex = 28;
            // 
            // lblNome_F
            // 
            this.lblNome_F.AutoSize = true;
            this.lblNome_F.BackColor = System.Drawing.Color.Transparent;
            this.lblNome_F.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.lblNome_F.Location = new System.Drawing.Point(97, 61);
            this.lblNome_F.Name = "lblNome_F";
            this.lblNome_F.Size = new System.Drawing.Size(61, 21);
            this.lblNome_F.TabIndex = 27;
            this.lblNome_F.Text = "Nome:";
            // 
            // txtNome_F
            // 
            this.txtNome_F.Location = new System.Drawing.Point(163, 62);
            this.txtNome_F.Name = "txtNome_F";
            this.txtNome_F.Size = new System.Drawing.Size(148, 20);
            this.txtNome_F.TabIndex = 26;
            // 
            // lblCadastrarCliente
            // 
            this.lblCadastrarCliente.AutoSize = true;
            this.lblCadastrarCliente.Font = new System.Drawing.Font("Century Gothic", 24F, System.Drawing.FontStyle.Bold);
            this.lblCadastrarCliente.ForeColor = System.Drawing.Color.DarkTurquoise;
            this.lblCadastrarCliente.Location = new System.Drawing.Point(141, 9);
            this.lblCadastrarCliente.Name = "lblCadastrarCliente";
            this.lblCadastrarCliente.Size = new System.Drawing.Size(352, 38);
            this.lblCadastrarCliente.TabIndex = 25;
            this.lblCadastrarCliente.Text = "Cadastrar funcionário";
            // 
            // lblUsuario_F
            // 
            this.lblUsuario_F.AutoSize = true;
            this.lblUsuario_F.BackColor = System.Drawing.Color.Transparent;
            this.lblUsuario_F.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.lblUsuario_F.Location = new System.Drawing.Point(87, 113);
            this.lblUsuario_F.Name = "lblUsuario_F";
            this.lblUsuario_F.Size = new System.Drawing.Size(70, 21);
            this.lblUsuario_F.TabIndex = 42;
            this.lblUsuario_F.Text = "Usuário:";
            // 
            // txtUsuario_F
            // 
            this.txtUsuario_F.Location = new System.Drawing.Point(163, 114);
            this.txtUsuario_F.Name = "txtUsuario_F";
            this.txtUsuario_F.Size = new System.Drawing.Size(148, 20);
            this.txtUsuario_F.TabIndex = 41;
            // 
            // lblCargo_F
            // 
            this.lblCargo_F.AutoSize = true;
            this.lblCargo_F.BackColor = System.Drawing.Color.Transparent;
            this.lblCargo_F.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.lblCargo_F.Location = new System.Drawing.Point(429, 114);
            this.lblCargo_F.Name = "lblCargo_F";
            this.lblCargo_F.Size = new System.Drawing.Size(64, 21);
            this.lblCargo_F.TabIndex = 46;
            this.lblCargo_F.Text = "Cargo:";
            // 
            // lblCPF_F
            // 
            this.lblCPF_F.AutoSize = true;
            this.lblCPF_F.BackColor = System.Drawing.Color.Transparent;
            this.lblCPF_F.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.lblCPF_F.Location = new System.Drawing.Point(449, 88);
            this.lblCPF_F.Name = "lblCPF_F";
            this.lblCPF_F.Size = new System.Drawing.Size(44, 21);
            this.lblCPF_F.TabIndex = 44;
            this.lblCPF_F.Text = "CPF:";
            // 
            // maskedTextBox2
            // 
            this.maskedTextBox2.Location = new System.Drawing.Point(499, 89);
            this.maskedTextBox2.Name = "maskedTextBox2";
            this.maskedTextBox2.Size = new System.Drawing.Size(115, 20);
            this.maskedTextBox2.TabIndex = 43;
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(499, 114);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(115, 21);
            this.comboBox1.TabIndex = 47;
            // 
            // comboBox2
            // 
            this.comboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Items.AddRange(new object[] {
            "Masculino",
            "Feminino"});
            this.comboBox2.Location = new System.Drawing.Point(499, 141);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(115, 21);
            this.comboBox2.TabIndex = 49;
            // 
            // lblSexo
            // 
            this.lblSexo.AutoSize = true;
            this.lblSexo.BackColor = System.Drawing.Color.Transparent;
            this.lblSexo.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.lblSexo.Location = new System.Drawing.Point(443, 141);
            this.lblSexo.Name = "lblSexo";
            this.lblSexo.Size = new System.Drawing.Size(50, 21);
            this.lblSexo.TabIndex = 48;
            this.lblSexo.Text = "Sexo:";
            // 
            // cboPermissao_Func
            // 
            this.cboPermissao_Func.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboPermissao_Func.FormattingEnabled = true;
            this.cboPermissao_Func.Location = new System.Drawing.Point(499, 166);
            this.cboPermissao_Func.Name = "cboPermissao_Func";
            this.cboPermissao_Func.Size = new System.Drawing.Size(115, 21);
            this.cboPermissao_Func.TabIndex = 53;
            // 
            // lblPermissao_Func
            // 
            this.lblPermissao_Func.AutoSize = true;
            this.lblPermissao_Func.BackColor = System.Drawing.Color.Transparent;
            this.lblPermissao_Func.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.lblPermissao_Func.Location = new System.Drawing.Point(404, 166);
            this.lblPermissao_Func.Name = "lblPermissao_Func";
            this.lblPermissao_Func.Size = new System.Drawing.Size(89, 21);
            this.lblPermissao_Func.TabIndex = 52;
            this.lblPermissao_Func.Text = "Permissão:";
            // 
            // frmCadastrarFuncionario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(634, 258);
            this.Controls.Add(this.cboPermissao_Func);
            this.Controls.Add(this.lblPermissao_Func);
            this.Controls.Add(this.comboBox2);
            this.Controls.Add(this.lblSexo);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.lblCargo_F);
            this.Controls.Add(this.lblCPF_F);
            this.Controls.Add(this.maskedTextBox2);
            this.Controls.Add(this.lblUsuario_F);
            this.Controls.Add(this.txtUsuario_F);
            this.Controls.Add(this.btnCadastrar_F);
            this.Controls.Add(this.lblEmail_F);
            this.Controls.Add(this.txtEmail_F);
            this.Controls.Add(this.DT_Nascimento);
            this.Controls.Add(this.lblData_Nascimento_F);
            this.Controls.Add(this.lblSenha_F);
            this.Controls.Add(this.txtSenha_F);
            this.Controls.Add(this.lblC_Senha_F);
            this.Controls.Add(this.txtC_Senha_F);
            this.Controls.Add(this.lblNome_F);
            this.Controls.Add(this.txtNome_F);
            this.Controls.Add(this.lblCadastrarCliente);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmCadastrarFuncionario";
            this.Text = "frmCadastrarFuncionario";
            this.Load += new System.EventHandler(this.frmCadastrarFuncionario_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCadastrar_F;
        private System.Windows.Forms.Label lblEmail_F;
        private System.Windows.Forms.MaskedTextBox txtEmail_F;
        private System.Windows.Forms.DateTimePicker DT_Nascimento;
        private System.Windows.Forms.Label lblData_Nascimento_F;
        private System.Windows.Forms.Label lblSenha_F;
        private System.Windows.Forms.MaskedTextBox txtSenha_F;
        private System.Windows.Forms.Label lblC_Senha_F;
        private System.Windows.Forms.MaskedTextBox txtC_Senha_F;
        private System.Windows.Forms.Label lblNome_F;
        private System.Windows.Forms.MaskedTextBox txtNome_F;
        private System.Windows.Forms.Label lblCadastrarCliente;
        private System.Windows.Forms.Label lblUsuario_F;
        private System.Windows.Forms.MaskedTextBox txtUsuario_F;
        private System.Windows.Forms.Label lblCargo_F;
        private System.Windows.Forms.Label lblCPF_F;
        private System.Windows.Forms.MaskedTextBox maskedTextBox2;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.Label lblSexo;
        private System.Windows.Forms.ComboBox cboPermissao_Func;
        private System.Windows.Forms.Label lblPermissao_Func;
    }
}