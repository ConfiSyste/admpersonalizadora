﻿namespace ProjetoADMPersonalizadora.Login.Menu.Folha_de_pagamento
{
    partial class frmGerarFolhadePagamento
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblTelefone_Forn = new System.Windows.Forms.Label();
            this.txtTelefone_Forn = new System.Windows.Forms.MaskedTextBox();
            this.txtCPF_Forn = new System.Windows.Forms.MaskedTextBox();
            this.lblCEP_Forn = new System.Windows.Forms.Label();
            this.txtUsuario_Forn = new System.Windows.Forms.MaskedTextBox();
            this.btnCadastrar_Forn = new System.Windows.Forms.Button();
            this.lblProduto_Forn = new System.Windows.Forms.Label();
            this.txtEmail_Forn = new System.Windows.Forms.MaskedTextBox();
            this.lblCNPJ_Forn = new System.Windows.Forms.Label();
            this.lblNome_Func = new System.Windows.Forms.Label();
            this.txtNome_Forn = new System.Windows.Forms.MaskedTextBox();
            this.lblFolhadePagamento = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblTelefone_Forn
            // 
            this.lblTelefone_Forn.AutoSize = true;
            this.lblTelefone_Forn.BackColor = System.Drawing.Color.Transparent;
            this.lblTelefone_Forn.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.lblTelefone_Forn.Location = new System.Drawing.Point(107, 98);
            this.lblTelefone_Forn.Name = "lblTelefone_Forn";
            this.lblTelefone_Forn.Size = new System.Drawing.Size(80, 21);
            this.lblTelefone_Forn.TabIndex = 103;
            this.lblTelefone_Forn.Text = "Telefone:";
            // 
            // txtTelefone_Forn
            // 
            this.txtTelefone_Forn.Location = new System.Drawing.Point(193, 98);
            this.txtTelefone_Forn.Name = "txtTelefone_Forn";
            this.txtTelefone_Forn.Size = new System.Drawing.Size(143, 20);
            this.txtTelefone_Forn.TabIndex = 102;
            // 
            // txtCPF_Forn
            // 
            this.txtCPF_Forn.Location = new System.Drawing.Point(193, 125);
            this.txtCPF_Forn.Name = "txtCPF_Forn";
            this.txtCPF_Forn.Size = new System.Drawing.Size(143, 20);
            this.txtCPF_Forn.TabIndex = 101;
            // 
            // lblCEP_Forn
            // 
            this.lblCEP_Forn.AutoSize = true;
            this.lblCEP_Forn.BackColor = System.Drawing.Color.Transparent;
            this.lblCEP_Forn.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.lblCEP_Forn.Location = new System.Drawing.Point(373, 98);
            this.lblCEP_Forn.Name = "lblCEP_Forn";
            this.lblCEP_Forn.Size = new System.Drawing.Size(45, 21);
            this.lblCEP_Forn.TabIndex = 100;
            this.lblCEP_Forn.Text = "CEP:";
            // 
            // txtUsuario_Forn
            // 
            this.txtUsuario_Forn.Location = new System.Drawing.Point(424, 97);
            this.txtUsuario_Forn.Name = "txtUsuario_Forn";
            this.txtUsuario_Forn.Size = new System.Drawing.Size(148, 20);
            this.txtUsuario_Forn.TabIndex = 99;
            // 
            // btnCadastrar_Forn
            // 
            this.btnCadastrar_Forn.Font = new System.Drawing.Font("Century Gothic", 20F);
            this.btnCadastrar_Forn.Location = new System.Drawing.Point(254, 171);
            this.btnCadastrar_Forn.Name = "btnCadastrar_Forn";
            this.btnCadastrar_Forn.Size = new System.Drawing.Size(167, 42);
            this.btnCadastrar_Forn.TabIndex = 98;
            this.btnCadastrar_Forn.Text = "Cadastrar";
            this.btnCadastrar_Forn.UseVisualStyleBackColor = true;
            // 
            // lblProduto_Forn
            // 
            this.lblProduto_Forn.AutoSize = true;
            this.lblProduto_Forn.BackColor = System.Drawing.Color.Transparent;
            this.lblProduto_Forn.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.lblProduto_Forn.Location = new System.Drawing.Point(342, 122);
            this.lblProduto_Forn.Name = "lblProduto_Forn";
            this.lblProduto_Forn.Size = new System.Drawing.Size(76, 21);
            this.lblProduto_Forn.TabIndex = 97;
            this.lblProduto_Forn.Text = "Produto:";
            // 
            // txtEmail_Forn
            // 
            this.txtEmail_Forn.Location = new System.Drawing.Point(424, 123);
            this.txtEmail_Forn.Name = "txtEmail_Forn";
            this.txtEmail_Forn.Size = new System.Drawing.Size(148, 20);
            this.txtEmail_Forn.TabIndex = 96;
            // 
            // lblCNPJ_Forn
            // 
            this.lblCNPJ_Forn.AutoSize = true;
            this.lblCNPJ_Forn.BackColor = System.Drawing.Color.Transparent;
            this.lblCNPJ_Forn.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.lblCNPJ_Forn.Location = new System.Drawing.Point(131, 124);
            this.lblCNPJ_Forn.Name = "lblCNPJ_Forn";
            this.lblCNPJ_Forn.Size = new System.Drawing.Size(56, 21);
            this.lblCNPJ_Forn.TabIndex = 95;
            this.lblCNPJ_Forn.Text = "CNPJ:";
            // 
            // lblNome_Func
            // 
            this.lblNome_Func.AutoSize = true;
            this.lblNome_Func.BackColor = System.Drawing.Color.Transparent;
            this.lblNome_Func.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.lblNome_Func.Location = new System.Drawing.Point(12, 72);
            this.lblNome_Func.Name = "lblNome_Func";
            this.lblNome_Func.Size = new System.Drawing.Size(175, 21);
            this.lblNome_Func.TabIndex = 94;
            this.lblNome_Func.Text = "nome do funcionário:";
            // 
            // txtNome_Forn
            // 
            this.txtNome_Forn.Location = new System.Drawing.Point(193, 72);
            this.txtNome_Forn.Name = "txtNome_Forn";
            this.txtNome_Forn.Size = new System.Drawing.Size(379, 20);
            this.txtNome_Forn.TabIndex = 93;
            // 
            // lblFolhadePagamento
            // 
            this.lblFolhadePagamento.AutoSize = true;
            this.lblFolhadePagamento.Font = new System.Drawing.Font("Century Gothic", 24F, System.Drawing.FontStyle.Bold);
            this.lblFolhadePagamento.ForeColor = System.Drawing.Color.DarkTurquoise;
            this.lblFolhadePagamento.Location = new System.Drawing.Point(185, 9);
            this.lblFolhadePagamento.Name = "lblFolhadePagamento";
            this.lblFolhadePagamento.Size = new System.Drawing.Size(351, 38);
            this.lblFolhadePagamento.TabIndex = 92;
            this.lblFolhadePagamento.Text = "Folha de pagamento:";
            // 
            // frmGerarFolhadePagamento
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(705, 426);
            this.Controls.Add(this.lblTelefone_Forn);
            this.Controls.Add(this.txtTelefone_Forn);
            this.Controls.Add(this.txtCPF_Forn);
            this.Controls.Add(this.lblCEP_Forn);
            this.Controls.Add(this.txtUsuario_Forn);
            this.Controls.Add(this.btnCadastrar_Forn);
            this.Controls.Add(this.lblProduto_Forn);
            this.Controls.Add(this.txtEmail_Forn);
            this.Controls.Add(this.lblCNPJ_Forn);
            this.Controls.Add(this.lblNome_Func);
            this.Controls.Add(this.txtNome_Forn);
            this.Controls.Add(this.lblFolhadePagamento);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmGerarFolhadePagamento";
            this.Text = "frmGerarFolhadePagamento";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblTelefone_Forn;
        private System.Windows.Forms.MaskedTextBox txtTelefone_Forn;
        private System.Windows.Forms.MaskedTextBox txtCPF_Forn;
        private System.Windows.Forms.Label lblCEP_Forn;
        private System.Windows.Forms.MaskedTextBox txtUsuario_Forn;
        private System.Windows.Forms.Button btnCadastrar_Forn;
        private System.Windows.Forms.Label lblProduto_Forn;
        private System.Windows.Forms.MaskedTextBox txtEmail_Forn;
        private System.Windows.Forms.Label lblCNPJ_Forn;
        private System.Windows.Forms.Label lblNome_Func;
        private System.Windows.Forms.MaskedTextBox txtNome_Forn;
        private System.Windows.Forms.Label lblFolhadePagamento;
    }
}