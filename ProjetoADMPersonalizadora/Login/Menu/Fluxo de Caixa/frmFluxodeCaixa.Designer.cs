﻿namespace ProjetoADMPersonalizadora.Login.Menu.Fluxo_de_Caixa
{
    partial class frmFluxodeCaixa
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblFluxodeCaixa = new System.Windows.Forms.Label();
            this.DT1Filtro_FluxodeCaixa = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.DT2Filtro_FluxodeCaixa = new System.Windows.Forms.DateTimePicker();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.btnPesquisar_fun = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // lblFluxodeCaixa
            // 
            this.lblFluxodeCaixa.AutoSize = true;
            this.lblFluxodeCaixa.Font = new System.Drawing.Font("Century Gothic", 24F, System.Drawing.FontStyle.Bold);
            this.lblFluxodeCaixa.ForeColor = System.Drawing.Color.DarkTurquoise;
            this.lblFluxodeCaixa.Location = new System.Drawing.Point(227, 9);
            this.lblFluxodeCaixa.Name = "lblFluxodeCaixa";
            this.lblFluxodeCaixa.Size = new System.Drawing.Size(253, 38);
            this.lblFluxodeCaixa.TabIndex = 54;
            this.lblFluxodeCaixa.Text = "Fluxo de caixa:";
            // 
            // DT1Filtro_FluxodeCaixa
            // 
            this.DT1Filtro_FluxodeCaixa.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.DT1Filtro_FluxodeCaixa.Location = new System.Drawing.Point(247, 62);
            this.DT1Filtro_FluxodeCaixa.Name = "DT1Filtro_FluxodeCaixa";
            this.DT1Filtro_FluxodeCaixa.Size = new System.Drawing.Size(96, 20);
            this.DT1Filtro_FluxodeCaixa.TabIndex = 57;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.label1.Location = new System.Drawing.Point(205, 61);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(36, 21);
            this.label1.TabIndex = 58;
            this.label1.Text = "De:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.label2.Location = new System.Drawing.Point(351, 61);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 21);
            this.label2.TabIndex = 60;
            this.label2.Text = "Até:";
            // 
            // DT2Filtro_FluxodeCaixa
            // 
            this.DT2Filtro_FluxodeCaixa.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.DT2Filtro_FluxodeCaixa.Location = new System.Drawing.Point(401, 62);
            this.DT2Filtro_FluxodeCaixa.Name = "DT2Filtro_FluxodeCaixa";
            this.DT2Filtro_FluxodeCaixa.Size = new System.Drawing.Size(96, 20);
            this.DT2Filtro_FluxodeCaixa.TabIndex = 59;
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(12, 129);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(655, 236);
            this.dataGridView1.TabIndex = 61;
            // 
            // btnPesquisar_fun
            // 
            this.btnPesquisar_fun.Font = new System.Drawing.Font("Century Gothic", 15F);
            this.btnPesquisar_fun.Location = new System.Drawing.Point(319, 88);
            this.btnPesquisar_fun.Name = "btnPesquisar_fun";
            this.btnPesquisar_fun.Size = new System.Drawing.Size(110, 35);
            this.btnPesquisar_fun.TabIndex = 71;
            this.btnPesquisar_fun.Text = "Pesquisar";
            this.btnPesquisar_fun.UseVisualStyleBackColor = true;
            // 
            // frmFluxodeCaixa
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(679, 377);
            this.Controls.Add(this.btnPesquisar_fun);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.DT2Filtro_FluxodeCaixa);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.DT1Filtro_FluxodeCaixa);
            this.Controls.Add(this.lblFluxodeCaixa);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmFluxodeCaixa";
            this.Text = "frmFluxodeCaixa";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lblFluxodeCaixa;
        private System.Windows.Forms.DateTimePicker DT1Filtro_FluxodeCaixa;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker DT2Filtro_FluxodeCaixa;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button btnPesquisar_fun;
    }
}