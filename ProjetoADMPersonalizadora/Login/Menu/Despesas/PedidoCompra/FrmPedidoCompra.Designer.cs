﻿namespace ProjetoADMPersonalizadora.Login.Menu.PedidoCompra
{
    partial class FrmPedidoCompra
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblQuantidade_Produto = new System.Windows.Forms.Label();
            this.cbbFornecedor = new System.Windows.Forms.ComboBox();
            this.txtValorCompra = new System.Windows.Forms.TextBox();
            this.lblValorCompra = new System.Windows.Forms.Label();
            this.lblFornecedorCompra = new System.Windows.Forms.Label();
            this.txtQuantidade_Produto = new System.Windows.Forms.TextBox();
            this.DT_Cadastro_F = new System.Windows.Forms.DateTimePicker();
            this.DTPedido_Compra = new System.Windows.Forms.Label();
            this.btnCalcular_Total_PC = new System.Windows.Forms.Button();
            this.lblRecuperação_de_senha = new System.Windows.Forms.Label();
            this.lblTotal_Reais = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblQuantidade_Produto
            // 
            this.lblQuantidade_Produto.AutoSize = true;
            this.lblQuantidade_Produto.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblQuantidade_Produto.Location = new System.Drawing.Point(30, 94);
            this.lblQuantidade_Produto.Name = "lblQuantidade_Produto";
            this.lblQuantidade_Produto.Size = new System.Drawing.Size(112, 21);
            this.lblQuantidade_Produto.TabIndex = 21;
            this.lblQuantidade_Produto.Text = "Quantidade:";
            // 
            // cbbFornecedor
            // 
            this.cbbFornecedor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbFornecedor.FormattingEnabled = true;
            this.cbbFornecedor.Location = new System.Drawing.Point(148, 67);
            this.cbbFornecedor.Name = "cbbFornecedor";
            this.cbbFornecedor.Size = new System.Drawing.Size(199, 21);
            this.cbbFornecedor.TabIndex = 20;
            // 
            // txtValorCompra
            // 
            this.txtValorCompra.Enabled = false;
            this.txtValorCompra.Location = new System.Drawing.Point(497, 68);
            this.txtValorCompra.Name = "txtValorCompra";
            this.txtValorCompra.Size = new System.Drawing.Size(96, 20);
            this.txtValorCompra.TabIndex = 17;
            // 
            // lblValorCompra
            // 
            this.lblValorCompra.AutoSize = true;
            this.lblValorCompra.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblValorCompra.Location = new System.Drawing.Point(437, 67);
            this.lblValorCompra.Name = "lblValorCompra";
            this.lblValorCompra.Size = new System.Drawing.Size(54, 21);
            this.lblValorCompra.TabIndex = 16;
            this.lblValorCompra.Text = "Valor:";
            // 
            // lblFornecedorCompra
            // 
            this.lblFornecedorCompra.AutoSize = true;
            this.lblFornecedorCompra.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFornecedorCompra.Location = new System.Drawing.Point(39, 68);
            this.lblFornecedorCompra.Name = "lblFornecedorCompra";
            this.lblFornecedorCompra.Size = new System.Drawing.Size(103, 21);
            this.lblFornecedorCompra.TabIndex = 13;
            this.lblFornecedorCompra.Text = "Fornecedor:";
            // 
            // txtQuantidade_Produto
            // 
            this.txtQuantidade_Produto.Enabled = false;
            this.txtQuantidade_Produto.Location = new System.Drawing.Point(148, 95);
            this.txtQuantidade_Produto.Name = "txtQuantidade_Produto";
            this.txtQuantidade_Produto.Size = new System.Drawing.Size(199, 20);
            this.txtQuantidade_Produto.TabIndex = 22;
            // 
            // DT_Cadastro_F
            // 
            this.DT_Cadastro_F.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.DT_Cadastro_F.Location = new System.Drawing.Point(497, 94);
            this.DT_Cadastro_F.MaxDate = new System.DateTime(5100, 12, 31, 0, 0, 0, 0);
            this.DT_Cadastro_F.MinDate = new System.DateTime(2018, 1, 31, 0, 0, 0, 0);
            this.DT_Cadastro_F.Name = "DT_Cadastro_F";
            this.DT_Cadastro_F.Size = new System.Drawing.Size(96, 20);
            this.DT_Cadastro_F.TabIndex = 53;
            // 
            // DTPedido_Compra
            // 
            this.DTPedido_Compra.AutoSize = true;
            this.DTPedido_Compra.BackColor = System.Drawing.Color.Transparent;
            this.DTPedido_Compra.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.DTPedido_Compra.Location = new System.Drawing.Point(353, 92);
            this.DTPedido_Compra.Name = "DTPedido_Compra";
            this.DTPedido_Compra.Size = new System.Drawing.Size(138, 21);
            this.DTPedido_Compra.TabIndex = 52;
            this.DTPedido_Compra.Text = "Data do Pedido:";
            // 
            // btnCalcular_Total_PC
            // 
            this.btnCalcular_Total_PC.Font = new System.Drawing.Font("Century Gothic", 13F);
            this.btnCalcular_Total_PC.Location = new System.Drawing.Point(241, 121);
            this.btnCalcular_Total_PC.Name = "btnCalcular_Total_PC";
            this.btnCalcular_Total_PC.Size = new System.Drawing.Size(139, 37);
            this.btnCalcular_Total_PC.TabIndex = 54;
            this.btnCalcular_Total_PC.Text = "Calcular Total";
            this.btnCalcular_Total_PC.UseVisualStyleBackColor = true;
            // 
            // lblRecuperação_de_senha
            // 
            this.lblRecuperação_de_senha.AutoSize = true;
            this.lblRecuperação_de_senha.Font = new System.Drawing.Font("Century Gothic", 24F, System.Drawing.FontStyle.Bold);
            this.lblRecuperação_de_senha.ForeColor = System.Drawing.Color.DarkTurquoise;
            this.lblRecuperação_de_senha.Location = new System.Drawing.Point(200, 9);
            this.lblRecuperação_de_senha.Name = "lblRecuperação_de_senha";
            this.lblRecuperação_de_senha.Size = new System.Drawing.Size(227, 38);
            this.lblRecuperação_de_senha.TabIndex = 55;
            this.lblRecuperação_de_senha.Text = "Fazer pedido:";
            // 
            // lblTotal_Reais
            // 
            this.lblTotal_Reais.AutoSize = true;
            this.lblTotal_Reais.Font = new System.Drawing.Font("Century Gothic", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotal_Reais.Location = new System.Drawing.Point(261, 164);
            this.lblTotal_Reais.Name = "lblTotal_Reais";
            this.lblTotal_Reais.Size = new System.Drawing.Size(99, 32);
            this.lblTotal_Reais.TabIndex = 56;
            this.lblTotal_Reais.Text = "R$0,00";
            // 
            // FrmPedidoCompra
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(612, 206);
            this.Controls.Add(this.lblTotal_Reais);
            this.Controls.Add(this.lblRecuperação_de_senha);
            this.Controls.Add(this.btnCalcular_Total_PC);
            this.Controls.Add(this.DT_Cadastro_F);
            this.Controls.Add(this.DTPedido_Compra);
            this.Controls.Add(this.txtQuantidade_Produto);
            this.Controls.Add(this.lblQuantidade_Produto);
            this.Controls.Add(this.cbbFornecedor);
            this.Controls.Add(this.txtValorCompra);
            this.Controls.Add(this.lblValorCompra);
            this.Controls.Add(this.lblFornecedorCompra);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FrmPedidoCompra";
            this.Text = "FrmPedidoCompra";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblQuantidade_Produto;
        private System.Windows.Forms.ComboBox cbbFornecedor;
        private System.Windows.Forms.TextBox txtValorCompra;
        private System.Windows.Forms.Label lblValorCompra;
        private System.Windows.Forms.Label lblFornecedorCompra;
        private System.Windows.Forms.TextBox txtQuantidade_Produto;
        private System.Windows.Forms.DateTimePicker DT_Cadastro_F;
        private System.Windows.Forms.Label DTPedido_Compra;
        private System.Windows.Forms.Button btnCalcular_Total_PC;
        private System.Windows.Forms.Label lblRecuperação_de_senha;
        private System.Windows.Forms.Label lblTotal_Reais;
    }
}