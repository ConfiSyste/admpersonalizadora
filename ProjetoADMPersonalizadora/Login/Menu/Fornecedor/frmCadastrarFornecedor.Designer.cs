﻿namespace ProjetoADMPersonalizadora.Login.Fornecedor
{
    partial class frmCadastrarFornecedor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblTelefone_Forn = new System.Windows.Forms.Label();
            this.txtTelefone_Forn = new System.Windows.Forms.MaskedTextBox();
            this.txtCPF_Forn = new System.Windows.Forms.MaskedTextBox();
            this.lblCEP_Forn = new System.Windows.Forms.Label();
            this.txtUsuario_Forn = new System.Windows.Forms.MaskedTextBox();
            this.btnCadastrar_Forn = new System.Windows.Forms.Button();
            this.lblProduto_Forn = new System.Windows.Forms.Label();
            this.txtEmail_Forn = new System.Windows.Forms.MaskedTextBox();
            this.lblCNPJ_Forn = new System.Windows.Forms.Label();
            this.lblRazão_Social_Forn = new System.Windows.Forms.Label();
            this.txtNome_Forn = new System.Windows.Forms.MaskedTextBox();
            this.lblCadastrarFornecedor = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblTelefone_Forn
            // 
            this.lblTelefone_Forn.AutoSize = true;
            this.lblTelefone_Forn.BackColor = System.Drawing.Color.Transparent;
            this.lblTelefone_Forn.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.lblTelefone_Forn.Location = new System.Drawing.Point(132, 98);
            this.lblTelefone_Forn.Name = "lblTelefone_Forn";
            this.lblTelefone_Forn.Size = new System.Drawing.Size(80, 21);
            this.lblTelefone_Forn.TabIndex = 91;
            this.lblTelefone_Forn.Text = "Telefone:";
            // 
            // txtTelefone_Forn
            // 
            this.txtTelefone_Forn.Location = new System.Drawing.Point(218, 98);
            this.txtTelefone_Forn.Name = "txtTelefone_Forn";
            this.txtTelefone_Forn.Size = new System.Drawing.Size(143, 20);
            this.txtTelefone_Forn.TabIndex = 90;
            // 
            // txtCPF_Forn
            // 
            this.txtCPF_Forn.Location = new System.Drawing.Point(218, 125);
            this.txtCPF_Forn.Name = "txtCPF_Forn";
            this.txtCPF_Forn.Size = new System.Drawing.Size(143, 20);
            this.txtCPF_Forn.TabIndex = 88;
            // 
            // lblCEP_Forn
            // 
            this.lblCEP_Forn.AutoSize = true;
            this.lblCEP_Forn.BackColor = System.Drawing.Color.Transparent;
            this.lblCEP_Forn.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.lblCEP_Forn.Location = new System.Drawing.Point(398, 98);
            this.lblCEP_Forn.Name = "lblCEP_Forn";
            this.lblCEP_Forn.Size = new System.Drawing.Size(45, 21);
            this.lblCEP_Forn.TabIndex = 87;
            this.lblCEP_Forn.Text = "CEP:";
            // 
            // txtUsuario_Forn
            // 
            this.txtUsuario_Forn.Location = new System.Drawing.Point(449, 97);
            this.txtUsuario_Forn.Name = "txtUsuario_Forn";
            this.txtUsuario_Forn.Size = new System.Drawing.Size(148, 20);
            this.txtUsuario_Forn.TabIndex = 86;
            // 
            // btnCadastrar_Forn
            // 
            this.btnCadastrar_Forn.Font = new System.Drawing.Font("Century Gothic", 20F);
            this.btnCadastrar_Forn.Location = new System.Drawing.Point(279, 171);
            this.btnCadastrar_Forn.Name = "btnCadastrar_Forn";
            this.btnCadastrar_Forn.Size = new System.Drawing.Size(167, 42);
            this.btnCadastrar_Forn.TabIndex = 85;
            this.btnCadastrar_Forn.Text = "Cadastrar";
            this.btnCadastrar_Forn.UseVisualStyleBackColor = true;
            // 
            // lblProduto_Forn
            // 
            this.lblProduto_Forn.AutoSize = true;
            this.lblProduto_Forn.BackColor = System.Drawing.Color.Transparent;
            this.lblProduto_Forn.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.lblProduto_Forn.Location = new System.Drawing.Point(367, 122);
            this.lblProduto_Forn.Name = "lblProduto_Forn";
            this.lblProduto_Forn.Size = new System.Drawing.Size(76, 21);
            this.lblProduto_Forn.TabIndex = 84;
            this.lblProduto_Forn.Text = "Produto:";
            // 
            // txtEmail_Forn
            // 
            this.txtEmail_Forn.Location = new System.Drawing.Point(449, 123);
            this.txtEmail_Forn.Name = "txtEmail_Forn";
            this.txtEmail_Forn.Size = new System.Drawing.Size(148, 20);
            this.txtEmail_Forn.TabIndex = 83;
            // 
            // lblCNPJ_Forn
            // 
            this.lblCNPJ_Forn.AutoSize = true;
            this.lblCNPJ_Forn.BackColor = System.Drawing.Color.Transparent;
            this.lblCNPJ_Forn.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.lblCNPJ_Forn.Location = new System.Drawing.Point(156, 124);
            this.lblCNPJ_Forn.Name = "lblCNPJ_Forn";
            this.lblCNPJ_Forn.Size = new System.Drawing.Size(56, 21);
            this.lblCNPJ_Forn.TabIndex = 81;
            this.lblCNPJ_Forn.Text = "CNPJ:";
            // 
            // lblRazão_Social_Forn
            // 
            this.lblRazão_Social_Forn.AutoSize = true;
            this.lblRazão_Social_Forn.BackColor = System.Drawing.Color.Transparent;
            this.lblRazão_Social_Forn.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.lblRazão_Social_Forn.Location = new System.Drawing.Point(102, 72);
            this.lblRazão_Social_Forn.Name = "lblRazão_Social_Forn";
            this.lblRazão_Social_Forn.Size = new System.Drawing.Size(110, 21);
            this.lblRazão_Social_Forn.TabIndex = 76;
            this.lblRazão_Social_Forn.Text = "Razão social:";
            // 
            // txtNome_Forn
            // 
            this.txtNome_Forn.Location = new System.Drawing.Point(218, 72);
            this.txtNome_Forn.Name = "txtNome_Forn";
            this.txtNome_Forn.Size = new System.Drawing.Size(379, 20);
            this.txtNome_Forn.TabIndex = 75;
            // 
            // lblCadastrarFornecedor
            // 
            this.lblCadastrarFornecedor.AutoSize = true;
            this.lblCadastrarFornecedor.Font = new System.Drawing.Font("Century Gothic", 24F, System.Drawing.FontStyle.Bold);
            this.lblCadastrarFornecedor.ForeColor = System.Drawing.Color.DarkTurquoise;
            this.lblCadastrarFornecedor.Location = new System.Drawing.Point(210, 9);
            this.lblCadastrarFornecedor.Name = "lblCadastrarFornecedor";
            this.lblCadastrarFornecedor.Size = new System.Drawing.Size(357, 38);
            this.lblCadastrarFornecedor.TabIndex = 74;
            this.lblCadastrarFornecedor.Text = "Cadastrar fornecedor:";
            // 
            // frmCadastrarFornecedor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(712, 258);
            this.Controls.Add(this.lblTelefone_Forn);
            this.Controls.Add(this.txtTelefone_Forn);
            this.Controls.Add(this.txtCPF_Forn);
            this.Controls.Add(this.lblCEP_Forn);
            this.Controls.Add(this.txtUsuario_Forn);
            this.Controls.Add(this.btnCadastrar_Forn);
            this.Controls.Add(this.lblProduto_Forn);
            this.Controls.Add(this.txtEmail_Forn);
            this.Controls.Add(this.lblCNPJ_Forn);
            this.Controls.Add(this.lblRazão_Social_Forn);
            this.Controls.Add(this.txtNome_Forn);
            this.Controls.Add(this.lblCadastrarFornecedor);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmCadastrarFornecedor";
            this.Text = "frmCadastrarFornecedor";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblTelefone_Forn;
        private System.Windows.Forms.MaskedTextBox txtTelefone_Forn;
        private System.Windows.Forms.MaskedTextBox txtCPF_Forn;
        private System.Windows.Forms.Label lblCEP_Forn;
        private System.Windows.Forms.MaskedTextBox txtUsuario_Forn;
        private System.Windows.Forms.Button btnCadastrar_Forn;
        private System.Windows.Forms.Label lblProduto_Forn;
        private System.Windows.Forms.MaskedTextBox txtEmail_Forn;
        private System.Windows.Forms.Label lblCNPJ_Forn;
        private System.Windows.Forms.Label lblRazão_Social_Forn;
        private System.Windows.Forms.MaskedTextBox txtNome_Forn;
        private System.Windows.Forms.Label lblCadastrarFornecedor;
    }
}