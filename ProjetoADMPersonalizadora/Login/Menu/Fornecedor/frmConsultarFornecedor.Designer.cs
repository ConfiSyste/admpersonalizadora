﻿namespace ProjetoADMPersonalizadora.Login.Menu.Fornecedor
{
    partial class txtRazaoSocial_Forn
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.lblRazaoSocial_Forn = new System.Windows.Forms.Label();
            this.lblCon_Fornecedor = new System.Windows.Forms.Label();
            this.txtFiltro_Forn = new System.Windows.Forms.TextBox();
            this.btnPesquisar_fun = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(12, 134);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(607, 278);
            this.dataGridView1.TabIndex = 68;
            // 
            // lblRazaoSocial_Forn
            // 
            this.lblRazaoSocial_Forn.AutoSize = true;
            this.lblRazaoSocial_Forn.BackColor = System.Drawing.Color.Transparent;
            this.lblRazaoSocial_Forn.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRazaoSocial_Forn.Location = new System.Drawing.Point(136, 75);
            this.lblRazaoSocial_Forn.Name = "lblRazaoSocial_Forn";
            this.lblRazaoSocial_Forn.Size = new System.Drawing.Size(110, 21);
            this.lblRazaoSocial_Forn.TabIndex = 63;
            this.lblRazaoSocial_Forn.Text = "Razão social:";
            // 
            // lblCon_Fornecedor
            // 
            this.lblCon_Fornecedor.AutoSize = true;
            this.lblCon_Fornecedor.Font = new System.Drawing.Font("Century Gothic", 24F, System.Drawing.FontStyle.Bold);
            this.lblCon_Fornecedor.ForeColor = System.Drawing.Color.DarkTurquoise;
            this.lblCon_Fornecedor.Location = new System.Drawing.Point(213, 9);
            this.lblCon_Fornecedor.Name = "lblCon_Fornecedor";
            this.lblCon_Fornecedor.Size = new System.Drawing.Size(235, 38);
            this.lblCon_Fornecedor.TabIndex = 62;
            this.lblCon_Fornecedor.Text = "Fornecedores:";
            // 
            // txtFiltro_Forn
            // 
            this.txtFiltro_Forn.Location = new System.Drawing.Point(252, 74);
            this.txtFiltro_Forn.Name = "txtFiltro_Forn";
            this.txtFiltro_Forn.Size = new System.Drawing.Size(170, 20);
            this.txtFiltro_Forn.TabIndex = 69;
            // 
            // btnPesquisar_fun
            // 
            this.btnPesquisar_fun.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.btnPesquisar_fun.Location = new System.Drawing.Point(428, 71);
            this.btnPesquisar_fun.Name = "btnPesquisar_fun";
            this.btnPesquisar_fun.Size = new System.Drawing.Size(76, 23);
            this.btnPesquisar_fun.TabIndex = 70;
            this.btnPesquisar_fun.Text = "Pesquisar";
            this.btnPesquisar_fun.UseVisualStyleBackColor = true;
            // 
            // txtRazaoSocial_Forn
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(631, 424);
            this.Controls.Add(this.btnPesquisar_fun);
            this.Controls.Add(this.txtFiltro_Forn);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.lblRazaoSocial_Forn);
            this.Controls.Add(this.lblCon_Fornecedor);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "txtRazaoSocial_Forn";
            this.Text = "frmConsultarFornecedor";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label lblRazaoSocial_Forn;
        private System.Windows.Forms.Label lblCon_Fornecedor;
        private System.Windows.Forms.TextBox txtFiltro_Forn;
        private System.Windows.Forms.Button btnPesquisar_fun;
    }
}