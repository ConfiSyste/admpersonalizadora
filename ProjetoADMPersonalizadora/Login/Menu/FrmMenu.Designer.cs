﻿namespace ProjetoADMPersonalizadora.Login.Menu
{
    partial class FrmMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.funcionáriosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.novoFuncionárioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.alterarFuncionárioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fornecedorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.novoFornecedorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarFornecedoresToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clienteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.novoClienteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarClientesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fluxoDeCaixaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.despesasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pedidoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.novoPedidoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarPedidosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.folhaDePagamentoToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.gerarFolhaDePagamentoToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarFolhasDePagamentoToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.ganhosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pedidoToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.novoPedidoToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarPedidosToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.orçamentoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gerarOrçamentoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarOrçamentoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.funcionáriosToolStripMenuItem,
            this.fornecedorToolStripMenuItem,
            this.clienteToolStripMenuItem,
            this.fluxoDeCaixaToolStripMenuItem,
            this.despesasToolStripMenuItem,
            this.ganhosToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(535, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // funcionáriosToolStripMenuItem
            // 
            this.funcionáriosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.novoFuncionárioToolStripMenuItem,
            this.alterarFuncionárioToolStripMenuItem});
            this.funcionáriosToolStripMenuItem.Name = "funcionáriosToolStripMenuItem";
            this.funcionáriosToolStripMenuItem.Size = new System.Drawing.Size(102, 20);
            this.funcionáriosToolStripMenuItem.Text = "&Funcionários";
            // 
            // novoFuncionárioToolStripMenuItem
            // 
            this.novoFuncionárioToolStripMenuItem.Name = "novoFuncionárioToolStripMenuItem";
            this.novoFuncionárioToolStripMenuItem.Size = new System.Drawing.Size(201, 22);
            this.novoFuncionárioToolStripMenuItem.Text = "Novo Funcionário";
            // 
            // alterarFuncionárioToolStripMenuItem
            // 
            this.alterarFuncionárioToolStripMenuItem.Name = "alterarFuncionárioToolStripMenuItem";
            this.alterarFuncionárioToolStripMenuItem.Size = new System.Drawing.Size(201, 22);
            this.alterarFuncionárioToolStripMenuItem.Text = "Alterar Funcionário";
            // 
            // fornecedorToolStripMenuItem
            // 
            this.fornecedorToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.novoFornecedorToolStripMenuItem,
            this.consultarFornecedoresToolStripMenuItem});
            this.fornecedorToolStripMenuItem.Name = "fornecedorToolStripMenuItem";
            this.fornecedorToolStripMenuItem.Size = new System.Drawing.Size(93, 20);
            this.fornecedorToolStripMenuItem.Text = "Fo&rnecedor";
            // 
            // novoFornecedorToolStripMenuItem
            // 
            this.novoFornecedorToolStripMenuItem.Name = "novoFornecedorToolStripMenuItem";
            this.novoFornecedorToolStripMenuItem.Size = new System.Drawing.Size(229, 22);
            this.novoFornecedorToolStripMenuItem.Text = "Novo Fornecedor";
            // 
            // consultarFornecedoresToolStripMenuItem
            // 
            this.consultarFornecedoresToolStripMenuItem.Name = "consultarFornecedoresToolStripMenuItem";
            this.consultarFornecedoresToolStripMenuItem.Size = new System.Drawing.Size(229, 22);
            this.consultarFornecedoresToolStripMenuItem.Text = "Consultar Fornecedores";
            // 
            // clienteToolStripMenuItem
            // 
            this.clienteToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.novoClienteToolStripMenuItem,
            this.consultarClientesToolStripMenuItem});
            this.clienteToolStripMenuItem.Name = "clienteToolStripMenuItem";
            this.clienteToolStripMenuItem.Size = new System.Drawing.Size(66, 20);
            this.clienteToolStripMenuItem.Text = "&Cliente";
            // 
            // novoClienteToolStripMenuItem
            // 
            this.novoClienteToolStripMenuItem.Name = "novoClienteToolStripMenuItem";
            this.novoClienteToolStripMenuItem.Size = new System.Drawing.Size(194, 22);
            this.novoClienteToolStripMenuItem.Text = "Novo Cliente";
            // 
            // consultarClientesToolStripMenuItem
            // 
            this.consultarClientesToolStripMenuItem.Name = "consultarClientesToolStripMenuItem";
            this.consultarClientesToolStripMenuItem.Size = new System.Drawing.Size(194, 22);
            this.consultarClientesToolStripMenuItem.Text = "Consultar Clientes";
            // 
            // fluxoDeCaixaToolStripMenuItem
            // 
            this.fluxoDeCaixaToolStripMenuItem.Name = "fluxoDeCaixaToolStripMenuItem";
            this.fluxoDeCaixaToolStripMenuItem.Size = new System.Drawing.Size(117, 20);
            this.fluxoDeCaixaToolStripMenuItem.Text = "Flu&xo de Caixa";
            // 
            // despesasToolStripMenuItem
            // 
            this.despesasToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pedidoToolStripMenuItem,
            this.folhaDePagamentoToolStripMenuItem1});
            this.despesasToolStripMenuItem.Name = "despesasToolStripMenuItem";
            this.despesasToolStripMenuItem.Size = new System.Drawing.Size(81, 20);
            this.despesasToolStripMenuItem.Text = "&Despesas";
            // 
            // pedidoToolStripMenuItem
            // 
            this.pedidoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.novoPedidoToolStripMenuItem,
            this.consultarPedidosToolStripMenuItem});
            this.pedidoToolStripMenuItem.Name = "pedidoToolStripMenuItem";
            this.pedidoToolStripMenuItem.Size = new System.Drawing.Size(210, 22);
            this.pedidoToolStripMenuItem.Text = "&Pedido";
            // 
            // novoPedidoToolStripMenuItem
            // 
            this.novoPedidoToolStripMenuItem.Name = "novoPedidoToolStripMenuItem";
            this.novoPedidoToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.novoPedidoToolStripMenuItem.Text = "&Novo Pedido";
            // 
            // consultarPedidosToolStripMenuItem
            // 
            this.consultarPedidosToolStripMenuItem.Name = "consultarPedidosToolStripMenuItem";
            this.consultarPedidosToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.consultarPedidosToolStripMenuItem.Text = "&Consultar Pedidos";
            // 
            // folhaDePagamentoToolStripMenuItem1
            // 
            this.folhaDePagamentoToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.gerarFolhaDePagamentoToolStripMenuItem1,
            this.consultarFolhasDePagamentoToolStripMenuItem1});
            this.folhaDePagamentoToolStripMenuItem1.Name = "folhaDePagamentoToolStripMenuItem1";
            this.folhaDePagamentoToolStripMenuItem1.Size = new System.Drawing.Size(210, 22);
            this.folhaDePagamentoToolStripMenuItem1.Text = "&Folha de Pagamento";
            // 
            // gerarFolhaDePagamentoToolStripMenuItem1
            // 
            this.gerarFolhaDePagamentoToolStripMenuItem1.Name = "gerarFolhaDePagamentoToolStripMenuItem1";
            this.gerarFolhaDePagamentoToolStripMenuItem1.Size = new System.Drawing.Size(282, 22);
            this.gerarFolhaDePagamentoToolStripMenuItem1.Text = "&Gerar Folha de Pagamento";
            // 
            // consultarFolhasDePagamentoToolStripMenuItem1
            // 
            this.consultarFolhasDePagamentoToolStripMenuItem1.Name = "consultarFolhasDePagamentoToolStripMenuItem1";
            this.consultarFolhasDePagamentoToolStripMenuItem1.Size = new System.Drawing.Size(282, 22);
            this.consultarFolhasDePagamentoToolStripMenuItem1.Text = "&Consultar Folhas de Pagamento";
            // 
            // ganhosToolStripMenuItem
            // 
            this.ganhosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pedidoToolStripMenuItem1,
            this.orçamentoToolStripMenuItem});
            this.ganhosToolStripMenuItem.Name = "ganhosToolStripMenuItem";
            this.ganhosToolStripMenuItem.Size = new System.Drawing.Size(70, 20);
            this.ganhosToolStripMenuItem.Text = "&Ganhos";
            this.ganhosToolStripMenuItem.Click += new System.EventHandler(this.ganhosToolStripMenuItem_Click);
            // 
            // pedidoToolStripMenuItem1
            // 
            this.pedidoToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.novoPedidoToolStripMenuItem1,
            this.consultarPedidosToolStripMenuItem1});
            this.pedidoToolStripMenuItem1.Name = "pedidoToolStripMenuItem1";
            this.pedidoToolStripMenuItem1.Size = new System.Drawing.Size(152, 22);
            this.pedidoToolStripMenuItem1.Text = "&Pedido";
            // 
            // novoPedidoToolStripMenuItem1
            // 
            this.novoPedidoToolStripMenuItem1.Name = "novoPedidoToolStripMenuItem1";
            this.novoPedidoToolStripMenuItem1.Size = new System.Drawing.Size(193, 22);
            this.novoPedidoToolStripMenuItem1.Text = "Novo Pedido";
            // 
            // consultarPedidosToolStripMenuItem1
            // 
            this.consultarPedidosToolStripMenuItem1.Name = "consultarPedidosToolStripMenuItem1";
            this.consultarPedidosToolStripMenuItem1.Size = new System.Drawing.Size(193, 22);
            this.consultarPedidosToolStripMenuItem1.Text = "Consultar Pedidos";
            // 
            // orçamentoToolStripMenuItem
            // 
            this.orçamentoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.gerarOrçamentoToolStripMenuItem,
            this.consultarOrçamentoToolStripMenuItem});
            this.orçamentoToolStripMenuItem.Name = "orçamentoToolStripMenuItem";
            this.orçamentoToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.orçamentoToolStripMenuItem.Text = "Orçamento";
            // 
            // gerarOrçamentoToolStripMenuItem
            // 
            this.gerarOrçamentoToolStripMenuItem.Name = "gerarOrçamentoToolStripMenuItem";
            this.gerarOrçamentoToolStripMenuItem.Size = new System.Drawing.Size(215, 22);
            this.gerarOrçamentoToolStripMenuItem.Text = "Gerar Orçamento";
            // 
            // consultarOrçamentoToolStripMenuItem
            // 
            this.consultarOrçamentoToolStripMenuItem.Name = "consultarOrçamentoToolStripMenuItem";
            this.consultarOrçamentoToolStripMenuItem.Size = new System.Drawing.Size(215, 22);
            this.consultarOrçamentoToolStripMenuItem.Text = "Consultar Orçamento";
            // 
            // FrmMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(535, 297);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "FrmMenu";
            this.Text = "MenuAdministrador";
            this.Load += new System.EventHandler(this.FrmMenu_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem funcionáriosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem novoFuncionárioToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem alterarFuncionárioToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fornecedorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem novoFornecedorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem consultarFornecedoresToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clienteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem novoClienteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem consultarClientesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fluxoDeCaixaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem despesasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pedidoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem novoPedidoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem consultarPedidosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ganhosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pedidoToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem novoPedidoToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem consultarPedidosToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem folhaDePagamentoToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem gerarFolhaDePagamentoToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem consultarFolhasDePagamentoToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem orçamentoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gerarOrçamentoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem consultarOrçamentoToolStripMenuItem;
    }
}