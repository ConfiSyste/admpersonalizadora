﻿namespace ProjetoADMPersonalizadora.Login.EsqueceuConta
{
    partial class FrmEsqueceuSenha
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblEmail_Usuario = new System.Windows.Forms.Label();
            this.txtEmail_Usuario = new System.Windows.Forms.MaskedTextBox();
            this.btnRecuperar_Senha = new System.Windows.Forms.Button();
            this.lblCPF = new System.Windows.Forms.Label();
            this.txtCPF = new System.Windows.Forms.MaskedTextBox();
            this.lblRecuperação_de_senha = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblEmail_Usuario
            // 
            this.lblEmail_Usuario.AutoSize = true;
            this.lblEmail_Usuario.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.lblEmail_Usuario.Location = new System.Drawing.Point(11, 54);
            this.lblEmail_Usuario.Name = "lblEmail_Usuario";
            this.lblEmail_Usuario.Size = new System.Drawing.Size(118, 21);
            this.lblEmail_Usuario.TabIndex = 11;
            this.lblEmail_Usuario.Text = "Email/Usuário:";
            // 
            // txtEmail_Usuario
            // 
            this.txtEmail_Usuario.Location = new System.Drawing.Point(135, 54);
            this.txtEmail_Usuario.Name = "txtEmail_Usuario";
            this.txtEmail_Usuario.Size = new System.Drawing.Size(142, 20);
            this.txtEmail_Usuario.TabIndex = 10;
            // 
            // btnRecuperar_Senha
            // 
            this.btnRecuperar_Senha.Font = new System.Drawing.Font("Century Gothic", 10F);
            this.btnRecuperar_Senha.Location = new System.Drawing.Point(283, 54);
            this.btnRecuperar_Senha.Name = "btnRecuperar_Senha";
            this.btnRecuperar_Senha.Size = new System.Drawing.Size(106, 46);
            this.btnRecuperar_Senha.TabIndex = 8;
            this.btnRecuperar_Senha.Text = "Recuperar Senha";
            this.btnRecuperar_Senha.UseVisualStyleBackColor = true;
            // 
            // lblCPF
            // 
            this.lblCPF.AutoSize = true;
            this.lblCPF.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.lblCPF.Location = new System.Drawing.Point(85, 79);
            this.lblCPF.Name = "lblCPF";
            this.lblCPF.Size = new System.Drawing.Size(44, 21);
            this.lblCPF.TabIndex = 13;
            this.lblCPF.Text = "CPF:";
            // 
            // txtCPF
            // 
            this.txtCPF.Location = new System.Drawing.Point(135, 80);
            this.txtCPF.Name = "txtCPF";
            this.txtCPF.Size = new System.Drawing.Size(142, 20);
            this.txtCPF.TabIndex = 12;
            // 
            // lblRecuperação_de_senha
            // 
            this.lblRecuperação_de_senha.AutoSize = true;
            this.lblRecuperação_de_senha.Font = new System.Drawing.Font("Century Gothic", 24F, System.Drawing.FontStyle.Bold);
            this.lblRecuperação_de_senha.ForeColor = System.Drawing.Color.DarkTurquoise;
            this.lblRecuperação_de_senha.Location = new System.Drawing.Point(12, 9);
            this.lblRecuperação_de_senha.Name = "lblRecuperação_de_senha";
            this.lblRecuperação_de_senha.Size = new System.Drawing.Size(389, 38);
            this.lblRecuperação_de_senha.TabIndex = 53;
            this.lblRecuperação_de_senha.Text = "Recuperação de senha:";
            // 
            // FrmEsqueceuSenha
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(412, 121);
            this.Controls.Add(this.lblRecuperação_de_senha);
            this.Controls.Add(this.lblCPF);
            this.Controls.Add(this.txtCPF);
            this.Controls.Add(this.lblEmail_Usuario);
            this.Controls.Add(this.txtEmail_Usuario);
            this.Controls.Add(this.btnRecuperar_Senha);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FrmEsqueceuSenha";
            this.Text = "FrmEsqueceuConta";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lblEmail_Usuario;
        private System.Windows.Forms.MaskedTextBox txtEmail_Usuario;
        private System.Windows.Forms.Button btnRecuperar_Senha;
        private System.Windows.Forms.Label lblCPF;
        private System.Windows.Forms.MaskedTextBox txtCPF;
        private System.Windows.Forms.Label lblRecuperação_de_senha;
    }
}