﻿using ProjetoADMPersonalizadora.Login.Menu;
using ProjetoADMPersonalizadora.Login.Menu.Funcionários;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjetoADMPersonalizadora
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new frmCadastrarFuncionario());
        }
    }
}
